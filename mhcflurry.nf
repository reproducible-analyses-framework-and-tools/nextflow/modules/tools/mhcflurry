process mhcflurry_predict {

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  label 'mhcflurry_container'
  label 'mhcflurry'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${norm_run}_${tumor_run}/mhcflurry"
  cache 'lenient'

  input:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path(inp_file)
  path data_dir
  val parstr

  output:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*mhcflurry.csv"), emit: mhcflurry_scores

  script:
  """
  mhcflurry-predict --models ${data_dir} --list-supported-alleles | sed 's/\\*//g' | sed 's/\$/\\\$/g' > supported_alleles

  head -1 ${inp_file} > input_file.supported.tsv

  tail -n +2 ${inp_file} | grep -f supported_alleles >> input_file.supported.tsv

  mhcflurry-predict \
    input_file.supported.tsv \
    --models ${data_dir} \
    ${parstr} \
    --out ${dataset}-${pat_name}-${norm_run}_${tumor_run}.mhcflurry.csv \
    > mhcflurry.log
  """
}

process mhcflurry_make_input_file {

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  label 'mhcflurry_container'
  label 'mhcflurry'
  cache 'lenient'

  input:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path(peptides), path(alleles)
  val comma_sep_lens

  output:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*mhcflurry_inp.tsv"), emit: mhcflurry_inpf

  script:
  """
#!/usr/bin/env python3

import itertools
import re

peptide_lens = [int(x) for x in "${comma_sep_lens}".split(',')]
print("Peptide lengths: {}".format(peptide_lens))


alleles = []
with open("${alleles}", 'r') as allele_inf:
    alleles = allele_inf.readline().rstrip().split(',')
print("Alleles: {}".format(alleles))

peptides = {}
with open("${peptides}", 'r') as pep_inf:
    for line in pep_inf:
       if re.search('^>', line):
           next(pep_inf) #Skipping comment line.
           peptides[line.rstrip().split(' ')[0].replace(':', '_').replace('>', '')] = next(pep_inf).rstrip()

def sliding_window(iterable, n):
    iterables = itertools.tee(iterable, n)

    for iterable, num_skipped in zip(iterables, itertools.count()):
        for _ in range(num_skipped):
            next(iterable, None)

    joind_peps = [''.join(x) for x in zip(*iterables)]

    return list(set(joind_peps))

pep_kmers = {}

for id, peptide in peptides.items():
    pep_kmers[id] = []
    if len(peptide) in peptide_lens:
        pep_kmers[id].append(''.join(peptide))
    else:
        for peptide_len in peptide_lens:
            pep_kmers[id].extend(sliding_window(peptide, peptide_len))

print("Peptide kmers: {}".format(pep_kmers))

with open("${dataset}-${pat_name}-${norm_run}_${tumor_run}.mhcflurry_inp.tsv", 'w') as outf:
    outf.write("identifier,peptide,allele\\n")
    for id, sub_pep_kmers in pep_kmers.items():
        for sub_pep_kmer in sub_pep_kmers:
            if not(re.search('\\*', sub_pep_kmer)):
                for allele in alleles:
                    allele = allele.replace("H-2-", "H2-") # Required for MHCFlurry allele nomenclature.
                    outf.write("{},{},{}\\n".format(id, sub_pep_kmer, allele))
  """
}
